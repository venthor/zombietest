﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace loopOwlDemo
{
    public class ManualInput : MonoBehaviour
    {
        private CharacterControl characterControl;

        void Awake()
        {
            characterControl = this.gameObject.GetComponent<CharacterControl>();
            characterControl.MoveRight = false;
            characterControl.MoveLeft = false;
            characterControl.MoveRightCarry = false;
            characterControl.MoveLeftCarry = false;
        }

        // Update is called once per frame
        void Update()
        {
            if(VirtualInputManager.Instance.MoveRight)
            {
                characterControl.MoveRight = true;
                characterControl.MoveRightCarry = true;
            }
            else
            {
                characterControl.MoveRight = false;
                characterControl.MoveRightCarry = false;
            }
            if(VirtualInputManager.Instance.MoveLeft)
            {
                
                characterControl.MoveLeft = true;
                characterControl.MoveLeftCarry = true;
            }
            else
            {
                characterControl.MoveLeft = false;
                characterControl.MoveLeftCarry = false;
            }
            if(VirtualInputManager.Instance.Crawl)
            {
                characterControl.Crawl = true;
            }
            else
            {
                characterControl.Crawl = false;
            }
            if (VirtualInputManager.Instance.PickUp)
            {
                characterControl.PickUp = true;
            }
            else
            {
                characterControl.PickUp = false;
            }
            if(VirtualInputManager.Instance.Throw)
            {
                characterControl.Throw = true;
            }
            else
            {
                characterControl.Throw = false;
            }
            if (VirtualInputManager.Instance.Feed)
            {
                characterControl.Feed = true;
            }
            else
            {
                characterControl.Feed = false;
            }
        }
    }
}
