﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace loopOwlDemo
{
    public class KeyboardInput : MonoBehaviour
    {
        void Update()
        {
            if (Input.GetKey(KeyCode.D))
            {

                VirtualInputManager.Instance.MoveRight = true;
            }
            else
            {
                VirtualInputManager.Instance.MoveRight = false;
            }
            if (Input.GetKey(KeyCode.A))
            {
               
                VirtualInputManager.Instance.MoveLeft = true;
            }
            else
            {
                VirtualInputManager.Instance.MoveLeft = false;
            }
            if (Input.GetKey(KeyCode.Space))
            {
                VirtualInputManager.Instance.Crawl = true;
            }
            else
            {
                VirtualInputManager.Instance.Crawl = false;
            }
            if (Input.GetKey(KeyCode.Q))
            {
                VirtualInputManager.Instance.PickUp = true;
            }
            else
            {
                VirtualInputManager.Instance.PickUp = false;
            }
            if (Input.GetKey(KeyCode.Space))
            {
                VirtualInputManager.Instance.Throw = true;
            }
            else
            {
                VirtualInputManager.Instance.Throw = false;
            }
            if (Input.GetKey(KeyCode.E))
            {
                VirtualInputManager.Instance.Feed = true;
            }
            else
            {
                VirtualInputManager.Instance.Feed = false;
            }
        }
    }
}

