﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Timer : MonoBehaviour
{
    public float TimeRemaining = 30;
    public Text timeText;
    public bool timerIsRunning = false;

    void Start()
    {
        timerIsRunning = true;
    }
    void Update()
    {
        if (TimeRemaining > 0)
        {
            TimeRemaining -= Time.deltaTime;
            DisplayTime(TimeRemaining);

        }
        else
        {
            TimeRemaining = 0;
            timerIsRunning = false;
        }
    }
    public void AddTime(float timeToAdd)
    {
        TimeRemaining += timeToAdd;
    }
    void DisplayTime(float timeDisplay)
    {
        timeDisplay = +1;
        float seconds = Mathf.FloorToInt(timeDisplay);
        timeText.text = string.Format("Time: {0:00}", TimeRemaining);
    }
}
