﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace loopOwlDemo
{
    public class CharacterStateBase : StateMachineBehaviour
    {
        
        public CharacterControl characterControl;
        public List<StateData> ListAbilityData = new List<StateData>();

        public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            foreach (StateData sd in ListAbilityData)
            {
                sd.OnEnter(this, animator, stateInfo);
            }
        }

        public void UpdateAll(CharacterStateBase characterStateBase, Animator animator, AnimatorStateInfo stateInfo)
        {
            foreach (StateData sd in ListAbilityData)
            {
                sd.UpdateAbility(characterStateBase, animator,stateInfo);
            }
        }
        public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            UpdateAll(this, animator,stateInfo);
        }
        public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            foreach (StateData sd in ListAbilityData)
            {
                sd.OnEnter(this, animator, stateInfo);
            }
        }

       
        public CharacterControl GetCharacterControl(Animator animator)
        {
            if (characterControl == null)
            {
                characterControl = animator.GetComponentInParent<CharacterControl>();
            }
            return characterControl;
        }


    }
}