﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace loopOwlDemo
{
    [CreateAssetMenu(fileName = "New State", menuName = "loopOwlDemo/AbilityData/ForceTransitionIdleCarry")]
    public class ForceTransitionIdleCarry : StateData
    {
        [Range(0.01f, 1f)]
        public float TransitionTiming;
        public override void OnEnter(CharacterStateBase characterStateBase, Animator animator, AnimatorStateInfo stateInfo)
        {

        }

        public override void OnExit(CharacterStateBase characterStateBase, Animator animator, AnimatorStateInfo stateInfo)
        {
            animator.SetBool(AnimationTransitionParameter.ForceTransitionIdleCarry.ToString(), false);
        }

        public override void UpdateAbility(CharacterStateBase characterStateBase, Animator animator, AnimatorStateInfo stateInfo)
        {
            if (stateInfo.normalizedTime >= TransitionTiming)
            {
                CharacterControl control = characterStateBase.GetCharacterControl(animator);
                //should always be true, but making sure zombie has rock to actually carry.
                if (control.animationProgress.HoldingWeapon != null)
                {
                    animator.SetBool(AnimationTransitionParameter.ForceTransitionIdleCarry.ToString(), true);
                }
                
            }
        }
    }
}

