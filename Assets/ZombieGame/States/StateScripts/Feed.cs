﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace loopOwlDemo
{
    
    [CreateAssetMenu(fileName = "New State", menuName = "loopOwlDemo/AbilityData/Feed")]
    public class Feed : StateData
    {
        private bool isAdded = false;
        public override void OnEnter(CharacterStateBase characterStateBase, Animator animator, AnimatorStateInfo stateInfo)
        {
            //gets called twice so I don't want to add 30secs
            if (!isAdded)
            {
                CharacterControl control = characterStateBase.GetCharacterControl(animator);
                control.Eat_Source.Play();
                control.AddToTimer(15f);
                isAdded = true;
            }
            else
            {
                isAdded = false;
            }
        }

        public override void OnExit(CharacterStateBase characterStateBase, Animator animator, AnimatorStateInfo stateInfo)
        {
           
            animator.SetBool(AnimationTransitionParameter.ForceFeedTransition.ToString(), false);
        }

        public override void UpdateAbility(CharacterStateBase characterStateBase, Animator animator, AnimatorStateInfo stateInfo)
        {
            animator.SetBool(AnimationTransitionParameter.ForceFeedTransition.ToString(), false);
            CharacterControl control = characterStateBase.GetCharacterControl(animator);

            if (control.CorpseCollied != null)
            {
                control.CorpseCollied.RaiseCorpse(control);
                //add 15secs to timer
               
                animator.SetBool(AnimationTransitionParameter.Feed.ToString(), false);
                if (control.animationProgress.EattingCorpse != null)
                {
                    control.animationProgress.EattingCorpse.DestroyCorpse();

                }
            }
        }
    }
}
