﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



namespace loopOwlDemo
{
    [CreateAssetMenu(fileName = "New State", menuName = "loopOwlDemo/AbilityData/Idle")]
    public class Idle : StateData
    {
        public override void OnEnter(CharacterStateBase characterStateBase, Animator animator, AnimatorStateInfo stateInfo)
        {
            
        }
        public override void UpdateAbility(CharacterStateBase characterStateBase, Animator animator, AnimatorStateInfo stateInfo)
        {
            CharacterControl control = characterStateBase.GetCharacterControl(animator);
            if(control.IsDead)
            {
                animator.SetBool(AnimationTransitionParameter.Death.ToString(), true);
                return;
            }
            if (control.PickUp)
            {
                if (IsNearLayeredObject(characterStateBase, animator,8))
                {
                    
                    animator.SetBool(AnimationTransitionParameter.PickUp.ToString(), true);
                }
                return;
            }
            if(control.Feed)
            {
                //check if corpse is near like weapon
                if (IsNearLayeredObject(characterStateBase, animator,11))
                {
                   
                    animator.SetBool(AnimationTransitionParameter.Feed.ToString(), true);
                }
                return;
            }
            if (control.MoveRight)
            {

                animator.SetBool(AnimationTransitionParameter.IsMoving.ToString(), true);
            }
            if (control.MoveLeft)
            {

                animator.SetBool(AnimationTransitionParameter.IsMoving.ToString(), true);
            }
        }
        public override void OnExit(CharacterStateBase characterStateBase, Animator animator, AnimatorStateInfo stateInfo)
        {
            
        }
        private bool IsNearLayeredObject(CharacterStateBase characterStateBase, Animator animator, int layerCheck)
        {
            CharacterControl control = characterStateBase.GetCharacterControl(animator);
            float BlockDistance = 0.6f;
            //check to see if weapon is in range to pick up!
            foreach (GameObject o in control.FrontSpheres)
            {
                Debug.DrawRay(o.transform.position, control.transform.forward * 0.5f, Color.yellow);
                RaycastHit hit;
                if (Physics.Raycast(o.transform.position, control.transform.forward, out hit, BlockDistance))
                {
                    //stepping on rock can pick up
                    if (hit.collider.gameObject.layer == layerCheck)
                    {
                        //need collider hit to assign weapon
                        if (layerCheck == 8)
                        {
                            control.WeaponCollided = hit.collider.gameObject.GetComponent<Weapon>();
                        }
                        if(layerCheck == 11)
                        {
                            control.CorpseCollied = hit.collider.gameObject.GetComponent<Corpse>();
                        }
                        return true;
                    }   
                }
            }
            control.WeaponCollided = null;
            control.CorpseCollied = null;
            return false;
        }
    }
}