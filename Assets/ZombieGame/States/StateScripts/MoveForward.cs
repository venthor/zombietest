﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace loopOwlDemo
{
    [CreateAssetMenu(fileName = "New State", menuName = "loopOwlDemo/AbilityData/MoveForward")]
    public class MoveForward : StateData
    {
        public float Speed;
        public float BlockDistance;
        private bool CanFeed = false;
        public override void OnEnter(CharacterStateBase characterStateBase, Animator animator, AnimatorStateInfo stateInfo)
        {

        }
        public override void UpdateAbility(CharacterStateBase characterStateBase, Animator animator, AnimatorStateInfo stateInfo)
        {
            CharacterControl control = characterStateBase.GetCharacterControl(animator);
            if(control.IsDead)
            {
                animator.SetBool(AnimationTransitionParameter.Death.ToString(), true);
                return;
            }
            if (control.MoveRight && control.MoveLeft)
            {
                animator.SetBool(AnimationTransitionParameter.IsMoving.ToString(), false);
                return;
            }
            if (!control.MoveRight && !control.MoveLeft)
            {
                animator.SetBool(AnimationTransitionParameter.IsMoving.ToString(), false);
                return;
            }
            if (control.MoveRight)
            {
                control.transform.rotation = Quaternion.Euler(0f, 0f, 0f);
                control.IsFacingForward = true;
                if (!CheckFront(control))
                {
                    control.transform.Translate(Vector3.forward * Speed * Time.deltaTime);
                }
                

            }
            if (control.MoveLeft)
            {
                control.transform.rotation = Quaternion.Euler(0f, 180f, 0f);
                control.IsFacingForward = true;
                if (!CheckFront(control))
                {
                    control.transform.Translate(Vector3.forward * Speed * Time.deltaTime);
                }
            }
            if (control.Feed && CanFeed)
            {
                animator.SetBool(AnimationTransitionParameter.Feed.ToString(), true);
            }
        }
        public override void OnExit(CharacterStateBase characterStateBase, Animator animator, AnimatorStateInfo stateInfo)
        {

        }
        bool CheckFront(CharacterControl control)
        {
            CanFeed = false;
            foreach (GameObject o in control.FrontSpheres)
            {
                //fun yellow lines to see what is colliding.  They are a bit longer than the Blockdistance for my old eyes.
                Debug.DrawRay(o.transform.position, control.transform.forward * 0.5f, Color.yellow);
                RaycastHit hit;
                if (Physics.Raycast(o.transform.position, control.transform.forward, out hit, BlockDistance))
                {
                    if (!control.RagDollParts.Contains(hit.collider))
                    {
                        //stepping on rock can pick up
                        if (hit.collider.gameObject.layer == 8)
                        {
                            return false;
                        }
                        if(hit.collider.gameObject.layer == 11)
                        {
                            //yummy I can eat!
                            //can step over corpses!!!--need to see the requirements??
                            CanFeed = true;
                            return false;
                        }
                        return true;
                    }
                }
            }
            return false;
        }

        bool IsBodyPart(Collider col)
        {
            CharacterControl control = col.transform.root.GetComponent<CharacterControl>();
            if (control == null)
            {
                return false;
            }
            //not a body part, its root of character
            if(control.gameObject == col.gameObject)
            {
                return false;
            }
            if (control.RagDollParts.Contains(col))
            {
                return true;
            }
            return false;
        }

    }
}