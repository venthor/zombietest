﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



namespace loopOwlDemo
{
    [CreateAssetMenu(fileName = "New State", menuName = "loopOwlDemo/AbilityData/ThrowRock")]
    public class ThrowRock : StateData
    {
        public override void OnEnter(CharacterStateBase characterStateBase, Animator animator, AnimatorStateInfo stateInfo)
        {

        }
        public override void UpdateAbility(CharacterStateBase characterStateBase, Animator animator, AnimatorStateInfo stateInfo)
        {
            CharacterControl control = characterStateBase.GetCharacterControl(animator);
            if (control.animationProgress.HoldingWeapon != null)
            {
                control.animationProgress.HoldingWeapon.ThrowWeapon(control);
                animator.SetBool(AnimationTransitionParameter.Throw.ToString(), false);
            }
        }
        public override void OnExit(CharacterStateBase characterStateBase, Animator animator, AnimatorStateInfo stateInfo)
        {

        }

    }
}