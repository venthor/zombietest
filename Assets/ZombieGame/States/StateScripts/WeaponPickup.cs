﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



namespace loopOwlDemo
{
    [CreateAssetMenu(fileName = "New State", menuName = "loopOwlDemo/AbilityData/WeaponPickup")]
    public class WeaponPickup : StateData
    {
        public float PickUpTiming;
        public override void OnEnter(CharacterStateBase characterStateBase, Animator animator, AnimatorStateInfo stateInfo)
        {

        }
        public override void UpdateAbility(CharacterStateBase characterStateBase, Animator animator, AnimatorStateInfo stateInfo)
        {
            CharacterControl control = characterStateBase.GetCharacterControl(animator);
            //we've already picked up the weapon
            if (control.WeaponCollided != null)
            {
                
                control.WeaponCollided.transform.localRotation = Quaternion.identity;
                control.animationProgress.HoldingWeapon = control.WeaponCollided;
                control.animationProgress.HoldingWeapon.IsHeld = true;
                control.animationProgress.HoldingWeapon.HoldWeapon(control);
                control.WeaponCollided = null;
            }
        }
        public override void OnExit(CharacterStateBase characterStateBase, Animator animator, AnimatorStateInfo stateInfo)
        {

        }

    }
}