﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace loopOwlDemo
{
    [CreateAssetMenu(fileName = "New State", menuName = "loopOwlDemo/AbilityData/ForceFeedTransition")]
    public class ForceFeedTransition : StateData
    {
        [Range(0.01f, 1f)]
        public float TransitionTiming;
        public override void OnEnter(CharacterStateBase characterStateBase, Animator animator, AnimatorStateInfo stateInfo)
        {
          
        }

        public override void OnExit(CharacterStateBase characterStateBase, Animator animator, AnimatorStateInfo stateInfo)
        {
           
            animator.SetBool(AnimationTransitionParameter.ForceFeedTransition.ToString(), false);
        }

        public override void UpdateAbility(CharacterStateBase characterStateBase, Animator animator, AnimatorStateInfo stateInfo)
        {
            if (stateInfo.normalizedTime >= TransitionTiming)
            {
                animator.SetBool(AnimationTransitionParameter.ForceFeedTransition.ToString(), true);
            }
        }
    }
}
