﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



namespace loopOwlDemo
{
    [CreateAssetMenu(fileName = "New State", menuName = "loopOwlDemo/AbilityData/GroundDector")]
    public class GroundDetector : StateData
    {
        [Range(0.01f, 1f)]
        public float CheckTime;
        public float Distance;
        public override void OnEnter(CharacterStateBase characterStateBase, Animator animator, AnimatorStateInfo stateInfo)
        {

        }
        public override void UpdateAbility(CharacterStateBase characterStateBase, Animator animator, AnimatorStateInfo stateInfo)
        {
            CharacterControl control = characterStateBase.GetCharacterControl(animator);
            //check to see if the character is grounded after a % of time has elapsed so its not always checking.
            if (stateInfo.normalizedTime >= CheckTime)
            {
                if (IsGrounded(control))
                {
                    animator.SetBool(AnimationTransitionParameter.Grounded.ToString(), true);
                }
                else
                {
                    animator.SetBool(AnimationTransitionParameter.Grounded.ToString(), false);
                }
            }
        }
        public override void OnExit(CharacterStateBase characterStateBase, Animator animator, AnimatorStateInfo stateInfo)
        {

        }
        bool IsGrounded(CharacterControl control)
        {
            if(control.RIGID_BODY.velocity.y > -0.001f && control.RIGID_BODY.velocity.y <=0f)
            {
                return true;
            }
            if (control.RIGID_BODY.velocity.y < 0f)
            {
                

                foreach (GameObject o in control.BottomSpheres)
                {
                    Vector3 collisonPoint = Vector3.zero;
                    GameObject blockingObj = CollisionDetection.GetCollidingObject(control, o, -Vector3.up, Distance,
                        ref  collisonPoint);

                    if (blockingObj != null)
                    {
                        RaycastHit hit;
                        if (Physics.Raycast(o.transform.position, -Vector3.up, out hit, Distance))
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }
    }
}