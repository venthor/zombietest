﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace loopOwlDemo
{
    [CreateAssetMenu(fileName = "New State", menuName = "loopOwlDemo/AbilityData/MoveForwardCarry")]
    public class MoveForwardCarry : StateData
    {
        public float Speed;
        public float BlockDistance;

        public override void OnEnter(CharacterStateBase characterStateBase, Animator animator, AnimatorStateInfo stateInfo)
        {

        }
        public override void UpdateAbility(CharacterStateBase characterStateBase, Animator animator, AnimatorStateInfo stateInfo)
        {
            CharacterControl control = characterStateBase.GetCharacterControl(animator);
            if (control.IsDead)
            {
                animator.SetBool(AnimationTransitionParameter.Death.ToString(), true);
                return;
            }
            if (control.Throw)
            {
           
                //no weapon to pick up.
                if (control.animationProgress.HoldingWeapon == null)
                {
                    return;
                }
                animator.SetBool(AnimationTransitionParameter.Throw.ToString(), true);
                return;
            }

            if (control.MoveRightCarry && control.MoveLeftCarry)
            {
                animator.SetBool(AnimationTransitionParameter.IsMovingCarry.ToString(), false);
                return;
            }
            if (!control.MoveRightCarry && !control.MoveLeftCarry)
            {
                animator.SetBool(AnimationTransitionParameter.IsMovingCarry.ToString(), false);
                return;
            }
            //keep carrying rock
            control.animationProgress.HoldingWeapon.HoldWeapon(control);

            if (control.MoveRightCarry)
            {
               
                control.transform.rotation = Quaternion.Euler(0f, 0f, 0f);
                control.IsFacingForward = true;
                control.transform.Translate(Vector3.forward * Speed * Time.deltaTime);
            }
            if (control.MoveLeftCarry)
            {
               
                control.transform.rotation = Quaternion.Euler(0f, 180f, 0f);
                control.IsFacingForward = false;
                control.transform.Translate(Vector3.forward * Speed * Time.deltaTime);
                
            }
        }
        public override void OnExit(CharacterStateBase characterStateBase, Animator animator, AnimatorStateInfo stateInfo)
        {
           
        }
        bool CheckFront(CharacterControl control)
        {


            foreach (GameObject o in control.FrontSpheres)
            {

                Debug.DrawRay(o.transform.position, control.transform.forward * 0.5f, Color.yellow);
                RaycastHit hit;
                if (Physics.Raycast(o.transform.position, control.transform.forward, out hit, BlockDistance))
                {
                    if (!control.RagDollParts.Contains(hit.collider))
                    {
                        if (!IsBodyPart(hit.collider))
                        {
                            Vector3 collisonPoint = Vector3.zero;

                            GameObject blockingObj = CollisionDetection.GetCollidingObject(control, o, o.transform.position, BlockDistance,
                        ref collisonPoint);
                            if (blockingObj == null)
                            {
                                return false;
                            }
                            else
                            {
                                return true;
                            }

                        }
                    }
                }
            }
            return false;
        }
        bool IsBodyPart(Collider col)
        {
            CharacterControl control = col.transform.root.GetComponent<CharacterControl>();
            if (control == null)
            {
                return false;
            }
            //not a body part, its root of character
            if (control.gameObject == col.gameObject)
            {
                return false;
            }
            if (control.RagDollParts.Contains(col))
            {
                return true;
            }
            return false;
        }

    }
}