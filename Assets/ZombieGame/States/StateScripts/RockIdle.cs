﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



namespace loopOwlDemo
{
    [CreateAssetMenu(fileName = "New State", menuName = "loopOwlDemo/AbilityData/RockIdle")]
    public class RockIdle : StateData
    {
        public override void OnEnter(CharacterStateBase characterStateBase, Animator animator, AnimatorStateInfo stateInfo)
        {

        }
        public override void UpdateAbility(CharacterStateBase characterStateBase, Animator animator, AnimatorStateInfo stateInfo)
        {
            CharacterControl control = characterStateBase.GetCharacterControl(animator);
            if (control.IsDead)
            {
                animator.SetBool(AnimationTransitionParameter.Death.ToString(), true);
                return;
            }
            if (control.Throw)
            {
               
                //no weapon to pick up.
                if (control.animationProgress.HoldingWeapon == null)
                {
                    return;
                }
                animator.SetBool(AnimationTransitionParameter.Throw.ToString(), true);
                return;
            }
            //keep carrying rock
            control.animationProgress.HoldingWeapon.HoldWeapon(control);

            if (control.MoveRightCarry)
            {
                
                animator.SetBool(AnimationTransitionParameter.IsMovingCarry.ToString(), true);
            }
            if (control.MoveLeftCarry)
            {

                animator.SetBool(AnimationTransitionParameter.IsMovingCarry.ToString(), true);
            }
        }
        public override void OnExit(CharacterStateBase characterStateBase, Animator animator, AnimatorStateInfo stateInfo)
        {

        }

    }
}