﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



namespace loopOwlDemo
{
    [CreateAssetMenu(fileName = "New State", menuName = "loopOwlDemo/AbilityData/Death")]
    public class Death : StateData
    {
        public override void OnEnter(CharacterStateBase characterStateBase, Animator animator, AnimatorStateInfo stateInfo)
        {

        }
        public override void UpdateAbility(CharacterStateBase characterStateBase, Animator animator, AnimatorStateInfo stateInfo)
        {
            
        }
        public override void OnExit(CharacterStateBase characterStateBase, Animator animator, AnimatorStateInfo stateInfo)
        {

        }

    }
}