﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace loopOwlDemo
{
    public class Corpse : MonoBehaviour
    {
        public bool IsGettingAte;
        public float Speed = 0.5f;
        public Vector3 InitialPosition;
        public Vector3 FinalPosition;
        private GameObject CorpseObject; 
        void Start()
        {
            IsGettingAte = false;
        }
        //put this to show corpse is moving as getting ate.  
        //I think shaking the corpse with some blood particles spilling would add more
        //scope creep
        public void RaiseCorpse(CharacterControl control)
        {
            if (control.CorpseCollied != null)
            {
                control.animationProgress.EattingCorpse = control.CorpseCollied;
                control.CorpseCollied = null;
                Corpse c = control.animationProgress.EattingCorpse;
                IsGettingAte = true;
                CorpseObject = control.animationProgress.EattingCorpse.gameObject;
                InitialPosition = CorpseObject.transform.root.position;
                FinalPosition = new Vector3(InitialPosition.x -1.0f, InitialPosition.y + 2.0f, InitialPosition.z);
           
            }
        }
       
        void Update()
        {
            if (IsGettingAte)
            {
                float step = Speed * Time.deltaTime;
                CorpseObject.transform.root.position = Vector3.MoveTowards(InitialPosition, FinalPosition, step);
                if (Vector3.Distance(CorpseObject.transform.position, FinalPosition) < 0.001f)
                {
                    //transfer the collided corpse to the eattig state
                
                    IsGettingAte = false;
                }
            }
        }

        public void DestroyCorpse()
        {
            
            Destroy(this.gameObject,5.5f);
        }

    }
}