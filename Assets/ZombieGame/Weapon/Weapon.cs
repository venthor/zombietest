﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace loopOwlDemo
{
    public class Weapon : MonoBehaviour
    {
        public bool IsThrown;
        public bool FlyForward;
        public float AngleOfRock;
        public float ForceApplied;
        public bool IsHeld;
       

        private Rigidbody rb;
        private BoxCollider box;
        private void Awake()
        {

            rb = this.gameObject.GetComponent<Rigidbody>();
            box = this.gameObject.GetComponent<BoxCollider>();
        }
        private void Start()
        {
            IsThrown = false;
        }
       
       
        public bool IsGrounded()
        {
            float dist = box.bounds.extents.y;
            dist += 0.01f;
            if (rb.velocity.y > -0.001f && rb.velocity.y <= 0f)
            {
                return true;
            }
            if (rb.velocity.y < 0f)
            {
                RaycastHit hit;
                if (Physics.Raycast(transform.position, -Vector3.up, out hit, dist))
                {
                    return true;
                }

            }
            return false;
        }

        public void HoldWeapon(CharacterControl control)
        {
            rb.isKinematic = false;


            Vector3 pos = control.transform.position;   
            transform.parent = control.transform;
            transform.position = pos;
            Vector3 newPos = new Vector3(pos.x, pos.y+1.2f, pos.z+.3f);
            transform.position = newPos;
            

        }
        public void ThrowWeapon(CharacterControl control)
        {
            Vector3 pos = transform.position;
            Vector3 ThrowOffset = new Vector3(pos.x, pos.y + 1.2f, pos.z + .3f);
            Weapon w = control.animationProgress.HoldingWeapon;
            if (w != null)
            {
                //no more holding
                w.transform.parent = null;

                FlyForward = control.IsFacingForward;
                IsThrown = true;
                rb.isKinematic = false;
                w.transform.rotation = Quaternion.identity;
                //start frame to throw rock once released from hand.
                w.transform.position = control.transform.position;
                w.transform.position += Vector3.up * ThrowOffset.y;
                w.transform.position += Vector3.forward * ThrowOffset.z;
                control.animationProgress.HoldingWeapon = null;

               
                box.isTrigger = false;
                Vector3 direction;
                Vector3 result = w.transform.forward;
                result.y = 0f;
                direction = result.sqrMagnitude == 0f ? Vector3.forward : result.normalized;
                direction = Quaternion.AngleAxis(AngleOfRock, Vector3.Cross(direction, Vector3.up)) * direction;
                rb.AddForce(direction * ForceApplied, ForceMode.Impulse);
                IsHeld = false;
                control.throw_source.Play();
            }
        }
    }
}
