﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace loopOwlDemo
{

    public class AnimationProgress : MonoBehaviour
    {
        
        public MoveForward LatestMoveForward;
        [Header("Ground Movement")]
        public bool IsIgnoreCharacterTime;

       
        [Header("Transition")]
        public bool LockTransition;

        [Header("Weapon")]
        public Weapon HoldingWeapon;

        [Header("Corpse")]
        public Corpse EattingCorpse;

        
        public Dictionary<CharacterControl, List<Collider>> CollidingWeapons =
           new Dictionary<CharacterControl, List<Collider>>();
        private void Awake()
        {
            HoldingWeapon = null;
            EattingCorpse = null;
        }
    }
}
