﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace loopOwlDemo
{
    public enum AnimationTransitionParameter
    {
        IsMoving,
        Grounded,
        PickUp,
        ForceTransition,
        ForceFeedTransition,
        ForceTransitionIdleCarry,
        ForceTransitionWalkCarry,
        IsMovingCarry,
        ForceTransitionThrow,
        Throw,
        Feed,
        Death,
    }
    public class CharacterControl : MonoBehaviour
    {
        
        public Animator CharacterAnimator;
        [Header("Character States")]
        public bool MoveRight;
        public bool MoveLeft;
        public bool MoveRightCarry;
        public bool MoveLeftCarry;
        public bool Crawl;
        public bool PickUp;
        public bool Throw;
        public bool Feed;
        public bool IsFacingForward;
        public bool IsDead;
        [Header("Objects")]
        public GameObject ColliderEdgePrefab;
        public BoxCollider boxCollider;
        public List<GameObject> BottomSpheres = new List<GameObject>();
        public List<GameObject> FrontSpheres = new List<GameObject>();
        public List<Collider> RagDollParts = new List<Collider>();
        public AnimationProgress animationProgress;
        private Rigidbody rigid;

        [Header("UI Elements")]
        public Text timeBox;
        private Timer timer;
        public Text deathText;
        public Text instructionText;

        
        [Header("Collison Items")]
        public Weapon WeaponCollided;
        public Corpse CorpseCollied;
        

        [Header("Setup")]
        public Animator SkinnedMeshAnimator;
        public GameObject RightHand_Attack;

        [Header("Sounds")]
        public AudioSource throw_source;
        public AudioSource Eat_Source;

        public Rigidbody RIGID_BODY
        {
            get
            {
                if (rigid == null)
                {
                    rigid = GetComponent<Rigidbody>();
                }
                return rigid;
            }
        }
      
        private void Awake()
        {
            IsDead = false;
            animationProgress = GetComponent<AnimationProgress>();
            SetRagDollParts();
            SetColliderSpheres();
            timer = timeBox.GetComponent<Timer>();
            deathText.enabled = false;
        }

        //give player time to keep moving by feeding
        public void AddToTimer(float timeToAdd)
        {
            timer.AddTime(timeToAdd);
        }
        private void Update()
        {
           
            if (!timer.timerIsRunning)
            {
                IsDead = true;
                deathText.enabled = true;
            }
        }
        private void SetRagDollParts()
        {
            Collider[] colliders = this.gameObject.GetComponentsInChildren<Collider>();
            foreach(Collider c in colliders)
            {
                if (c.gameObject != this.gameObject)
                {
                    c.isTrigger = true;
                    RagDollParts.Add(c);
                }
            }
        }

      

        private void TurnOnRagDollParts()
        {
            RIGID_BODY.useGravity = false;
            RIGID_BODY.velocity = Vector3.zero;
            this.gameObject.GetComponent<BoxCollider>().enabled = false;
            CharacterAnimator.enabled = false;
            CharacterAnimator.avatar = null;
            foreach (Collider c in RagDollParts)
            {
                c.isTrigger = false;
                c.attachedRigidbody.velocity = Vector3.zero;
            }
        }
        private void SetColliderSpheres()
        {
            


            float bottom = boxCollider.bounds.center.y - boxCollider.bounds.extents.y ;
            float top = boxCollider.bounds.center.y + boxCollider.bounds.extents.y ;
            float front = boxCollider.bounds.center.z + boxCollider.bounds.extents.z ;
            float back = boxCollider.bounds.center.z - boxCollider.bounds.extents.z;

            GameObject bottomFront = CreateEdgeSphere(new Vector3(this.transform.position.x, bottom, front));
            GameObject bottomBack = CreateEdgeSphere(new Vector3(this.transform.position.x, bottom, back));
            GameObject topFront = CreateEdgeSphere(new Vector3(this.transform.position.x, top, front));

            bottomFront.transform.parent = this.transform;
            bottomBack.transform.parent = this.transform;
            topFront.transform.parent = this.transform;

            BottomSpheres.Add(bottomFront);
            BottomSpheres.Add(bottomBack);
            FrontSpheres.Add(bottomFront);
            FrontSpheres.Add(topFront);



            //create 5 spheres equal distant from front to back along the bottom of the collider box

            float horiztonalSect = (bottomFront.transform.position - bottomBack.transform.position).magnitude / 5f;
            CreateMiddleSpheres(bottomFront, -this.transform.forward, horiztonalSect, 4, BottomSpheres);

            float verticalSect = (bottomFront.transform.position - topFront.transform.position).magnitude / 10f;
            CreateMiddleSpheres(bottomFront, this.transform.up, verticalSect, 9, FrontSpheres);
        }
        public void CreateMiddleSpheres(GameObject start, Vector3 dir, float sec, int interations, List<GameObject> SphereList)
        {
            for (int i = 0; i < interations; i++)
            {
                Vector3 pos = start.transform.position + (dir * sec * (i + 1));
                GameObject newObj = CreateEdgeSphere(pos);
                newObj.transform.parent = boxCollider.transform;
                SphereList.Add(newObj);
            }
        }
        public GameObject CreateEdgeSphere(Vector3 postion)
        {
            GameObject obj = Instantiate(ColliderEdgePrefab, postion, Quaternion.identity);
            return obj;
        }
       
    }
}
