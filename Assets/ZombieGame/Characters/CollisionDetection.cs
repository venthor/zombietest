﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace loopOwlDemo
{
    public class CollisionDetection : MonoBehaviour
    {

        //TODO
        //Can get rid of this method.  Needs refactoring to use what is in Idle/WeaponPickup....this was initial 
        //pass at getting multiple collided objects that I would store in a dictionary, but refactored away from that.
        //If I have time before I submit
       
        public static GameObject GetCollidingObject(CharacterControl control, GameObject start, Vector3 dir, float blockDistance, ref Vector3 collisonPoint)
        {
            collisonPoint = Vector3.zero;
            Debug.DrawRay(start.transform.position, dir * blockDistance, Color.yellow);
          
            RaycastHit hit;
            if (Physics.Raycast(start.transform.position, dir, out hit, blockDistance))
            {
                
                if(hit.collider.gameObject.layer == 8)
                {
                   
                    return hit.collider.transform.root.gameObject;
                }
                else
                {
                    return null;
                }
              
            }
            else
            {
                
                return null;
            }

        }

       
        
    }
}
