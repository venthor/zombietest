﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace loopOwlDemo
{
    //Singleton class only one instance to control input behavior
    public class VirtualInputManager : Singleton<VirtualInputManager>
    {
        
        public bool MoveRight;
        public bool MoveLeft;
        public bool Crawl;
        public bool PickUp;
        public bool Throw;
        public bool Feed;
    }
}
