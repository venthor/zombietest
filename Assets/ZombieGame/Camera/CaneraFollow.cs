﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace loopOwlDemo
{
    public class CaneraFollow : MonoBehaviour
    {

        public Transform Player;
        public float camZ = 10.0f;

        public float smoothing = 5f;       
        public float cameraYPosition = 4f;
        public float cameraXPostion = 15.0f;
        private Transform target;
        private Vector3 offset;
       
        void Start()
        {
            Configure();
        }
        void Update()
        {
            if (target != null)
            {
                Vector3 targetCamPos = target.position + offset;
                transform.position = Vector3.Lerp(transform.position, targetCamPos, smoothing * Time.deltaTime);
            }
            
        }

        public void Configure()
        {
            Vector3 newCameraPosition = Player.position;
            newCameraPosition.z = camZ;
            newCameraPosition.x = newCameraPosition.x + cameraXPostion;
            newCameraPosition.y = newCameraPosition.y + cameraYPosition;
            transform.position = newCameraPosition;
            target = Player;
            offset = transform.position - target.position;
        }

    }
}